import { Component, Input } from "@angular/core";
import { Field } from "src/app/classes/field";
import { ConfirmationService } from "primeng/api";

@Component({
    selector: 'control-options',
    templateUrl: './control-options.component.html',
    styles: [`
        .card-body {
            padding: 5px;
            background-color: #f5f6f5;
        }

        .btn-toolbar {
            margin-bottom: 6px; 
            float: right;
        }

        .add-option-input {
            width: 100%;
            border-top-right-radius: 0px;
            border-bottom-right-radius: 0px;
        }
    `]
})
export class ControlOptionsComponent {

    @Input() field: Field;

    public selectedOptions: Array<string>;
    public newOption: string;

    public constructor(private confirmationService: ConfirmationService) {
    }

    public deleteOption() {
        this.field.options = this.field.options.filter(o => this.selectedOptions.indexOf(o) == -1);
    }

    public moveUp() {
        let minIndex = this.field.options.length - 1;
        this.selectedOptions.forEach(o => {
            let index = this.field.options.indexOf(o);
            minIndex = index < minIndex ? index : minIndex;
        });
        let front = this.field.options.filter((f,i) => i < minIndex-1);
        let end = this.field.options.filter(f => front.indexOf(f) == -1);
        end = end.filter(f => this.selectedOptions.indexOf(f) == -1);
        let newArray = new Array<string>();
        this.field.options = newArray.concat(front).concat(this.selectedOptions).concat(end);
    }

    public moveDown() {
        let maxIndex = 0;
        this.selectedOptions.forEach(o => {
            let index = this.field.options.indexOf(o);
            maxIndex = index > maxIndex ? index : maxIndex;
        });
        let end = this.field.options.filter((f,i) => i > maxIndex+1);
        let front = this.field.options.filter(f => end.indexOf(f) == -1);
        front = front.filter(f => this.selectedOptions.indexOf(f) == -1);
        let newArray = new Array<string>();
        this.field.options = newArray.concat(front).concat(this.selectedOptions).concat(end);
    }

    public clearOptions() {
        this.confirmationService.confirm({
            message: '¿Querés eliminar todas las opciones?',
            acceptLabel: 'Sí',
            rejectLabel: 'No',
            accept: () => {
                this.field.options = new Array<string>();
            }
        });
    }

    public addOption(): void {
        this.field.options.push(this.newOption);
        this.newOption = undefined;
    }

}