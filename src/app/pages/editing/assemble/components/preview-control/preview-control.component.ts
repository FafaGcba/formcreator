import { Component, Input } from "@angular/core";
import { Field } from "src/app/classes/field";

@Component({
    selector: 'preview-control',
    templateUrl: './preview-control.component.html',
    styles: [`
        textarea {
            resize: none;
        }

        .ui-fluid {
            width: 100%;
        }

        .ui-g-12 {
            padding: 0px;
            margin-top: -30px;
        }

    `]
})
export class PreviewControlComponent {

    @Input() field: Field;

    public minYear: number;
    public maxYear: number;
    public localeCalendarConfig: {};

    constructor() {
        this.maxYear = new Date().getFullYear();
        this.minYear = this.maxYear - 110;
        this.localeCalendarConfig = {
            firstDayOfWeek: 1,
            dayNames: [ "Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado" ],
            dayNamesShort: [ "dom","lun","mar","mié","jue","vie","sáb"],
            dayNamesMin: [ "D","L","M","X","J","V","S" ],
            monthNames: [ "Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre" ],
            monthNamesShort: [ "ene","feb","mar","abr","may","jun","jul","ago","sep","oct","nov","dic" ],
            today: 'Hoy',
            clear: 'Borrar'
        }
    }

}