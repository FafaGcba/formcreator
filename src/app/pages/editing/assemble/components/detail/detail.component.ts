import { Component, Input, OnInit, OnDestroy } from "@angular/core";
import { Field } from "src/app/classes/field";
import { ConfirmationService } from "primeng/api";
import { BehaviorSubject, Subscription } from "rxjs";
import { Validations } from "src/app/classes/validations";

@Component({
    selector: 'detail',
    templateUrl: 'detail.component.html',
    styles: [`
        label {
            font-size: 16px;
            margin-top: 16px;
            margin-bottom: 2px;
        }
    `]
})
export class DetailComponent implements OnInit, OnDestroy{
        
    @Input('show') $show: BehaviorSubject<boolean>;
    @Input('field') set field(f: Field) { this.onFieldChange(f) };
    
    public showSub: Subscription;
    public show: boolean;
    public originalField: Field;
    public editingField: Field = new Field();
    
    public isRequired: boolean = false;

    constructor(private confirmationService: ConfirmationService) {
    }

    ngOnInit(): void {
        this.showSub = this.$show.subscribe( v => this.show = v );
    }

    ngOnDestroy(): void {
        this.showSub.unsubscribe();
    }

    public onFieldChange(f: Field): void {
        if(f) {
            this.originalField = f;
            this.editingField = new Field();
            this.editingField.asignProps(f);
            if(this.originalField.validations.filter(v => v.type == 'Required').length > 0) {
                this.isRequired = true;
            }
        }
    }

    public showAttribute(attr: String): boolean {
        if(attr == 'Placeholder') {
            return this.editingField.renderType == 'FormTextController' 
            || this.editingField.renderType == 'FormNumberController';
        } else if(attr == 'Options') {
            return this.editingField.renderType == 'FormSelectController' 
            || this.editingField.renderType == 'FormRadiobuttonController';
        }
    }

    public save(): void {
        this.confirmationService.confirm({
            message: '¿Querés guardar los cambios?',
            acceptLabel: 'Sí',
            rejectLabel: 'No',
            accept: () => {
                if(this.isRequired) {
                    //let newVal = new Validations();
                    //newVal.type = 'Required';
                    //this.editingField.validations.push(newVal);
                }
                this.originalField.asignProps(this.editingField);
                this.$show.next(false);
            }
        });
    }

    public cancel(): void {
        this.confirmationService.confirm({
            message: '¿Seguro que querés cancelar? Los cambios que hayas hecho se van a perder',
            acceptLabel: 'Sí',
            rejectLabel: 'No',
            accept: () => {
                this.$show.next(false);
                this.onFieldChange(this.originalField);
            }
        });
    }

}
