import { Component, Input } from "@angular/core";
import { Validations, Params } from "src/app/classes/validations";
import { Field } from "src/app/classes/field";

@Component({
    selector: 'validations',
    templateUrl: './validations.component.html',
    styles: [`
        .fa-times {
            float: right; 
            font-size: 1.2em; 
        }
    `]
})
export class ValidationsComponent {

    @Input() field: Field;
    
    public showDialog: boolean = false;
    public availableValidations: Array<String>;
    public selectedValidation: String;
    public currentParams: Params;

    public titles: Map<String, String>;

    public constructor() {
        this.titles = new Map<String, String>();
        this.titles.set('dni', 'DNI');
        this.titles.set('email', 'Email');
        this.titles.set('max', 'Valor máximo');
        this.titles.set('maxLength', 'Cantidad máxima de carácteres');
        this.titles.set('min', 'Valor mínimo');
        this.titles.set('minLength', 'Cantidad mínima de carácteres');
        this.titles.set('numeric', 'Contiene un valor numérico');
        this.titles.set('required', 'Es obligatorio');

        this.availableValidations = Array.from(this.titles.keys());
        this.selectedValidation = 'dni';
        this.currentParams = new Params();
    }

    public doShowDialog(): void {
        this.currentParams = new Params();
        this.showDialog = true;
    }

    public addValidation(): void {
        this.availableValidations = this.availableValidations.filter(av => av != this.selectedValidation);
        this.field.validations.push(new Validations(this.selectedValidation, this.currentParams));
        this.selectedValidation = this.availableValidations[0];
        this.showDialog = false;
    }

    public getParamsDetail(val: Validations): String {
        let typesWithParams = ['max', 'maxLength', 'min', 'minLength'];
        if(typesWithParams.indexOf(val.type as string) > -1) {
            return '(' + val.params[val.type as string] + ')';
        }
        return '';
    }

    public removeValidation(val: Validations) {
        this.availableValidations.push(val.type);
        this.availableValidations.sort((a, b) => a.localeCompare(b as string));
        this.selectedValidation = this.availableValidations[0];
        this.field.validations = this.field.validations.filter(va => va != val);
    }

}
