import { Component, Input } from '@angular/core';
import { Field } from 'src/app/classes/field';
import { BehaviorSubject } from 'rxjs';
import { ConfirmationService } from 'primeng/api';
import { FieldsList } from './fields.list';

@Component({
  selector: 'assemble',
  templateUrl: 'assemble.component.html',
  styles: [`

    .list-group-item {
        cursor: pointer;
    }

    .list-group-item:hover {
        background-color: #ececec;
    }

  `]
})
export class AssembleComponent {
    
    @Input('fields') formFields: BehaviorSubject<Field[]>;

    public showDialog: boolean = false;
    public availableFields: Array<{field:string, name:string}>;
    public selectedNewField: string;

    public showDetail: BehaviorSubject<boolean> = new BehaviorSubject(false);
    public selectedField: Field;

    public constructor(private confirmationService: ConfirmationService) {
        this.availableFields = FieldsList;
        this.selectedNewField = this.availableFields[0].field;
    }

    ngOnInit() {
        this.showDetail.subscribe(f => {
            let newList = new Array<Field>();
            this.formFields.value.forEach(f => {
                newList.push(f);
            });
            this.formFields.next(newList);
        });
    }

    public clickAddField(): void {
        this.availableFields = FieldsList.filter(f => {
            return !this.formFields.value.find(ff => ff.cmField == f.field);
        });
        this.selectedNewField = this.availableFields[0].field;
        this.showDialog =  true;
    }

    public doAddField() {
        let name = FieldsList.filter(f => f.field == this.selectedNewField)[0].name;
        let newField = new Field();
        newField.renderType = "FormTextController";
        newField.cmField = this.selectedNewField;
        newField.name = name.replace(/ /g,'');
        newField.label = name
        newField.placeholder = name;
        newField.options = [];
        this.formFields.value.push(newField);
        this.showDialog = false;
    }

    /*public dragStart(control: Control) {
        this.draggedControl = control;
    }*/

    /*public drop() {
        if(this.draggedControl) {
            
        }
    }*/

    public findIndex(field: Field) {
        let index = -1;
        for(let i = 0; i < this.availableFields.length; i++) {
            /*if(field.cmField === this.availableFields[i].cmField) {
                index = i;
                break;
            } */
        }
        return index;
    }

    public openOverlay(op1: any, $event: any, field: Field) {
        op1.toggle($event);
        this.selectedField = field;
    }

    public doShowDetail() {
        this.showDetail.next(true);
    }

    public removeFromForm() {
        this.confirmationService.confirm({
            message: '¿Seguro que querés eliminar este campo?',
            acceptLabel: 'Sí',
            rejectLabel: 'No',
            accept: () => {
                this.formFields.next(this.formFields.value.filter(f => f!=this.selectedField));
            }
        });
    }
}
