import { Component } from "@angular/core";
import { FormConfiguration } from "src/app/classes/formConfiguration";

@Component({
    selector: 'parameters',
    templateUrl: 'parameters.component.html'
})
export class ParametersComponent {

    public formConfiguration: FormConfiguration;

    public invalidTitle: boolean;
    public invalidImgLink: boolean;
    public invalidThanks: boolean;

    public constructor() {
        this.formConfiguration = new FormConfiguration();
        this.invalidTitle = false;
        this.invalidImgLink = false;
        this.invalidThanks = false;
    }

}