import { Component } from "@angular/core";
import { Field } from "src/app/classes/field";
import { BehaviorSubject } from "rxjs";
import { FormConfiguration } from "src/app/classes/formConfiguration";
import { Modal } from 'ngx-modialog/plugins/bootstrap';

@Component({
    selector: 'editing',
    templateUrl: 'editing.component.html',
    styles: [`
        .btn-group {
            margin-top: 10px;
            margin-bottom: -10px;
        }

        .btn-custom {
            border: solid 0.5px #cccccc !important;
            box-shadow: none;
        }

        .section-selected {
            background-color: #00b3e3 !important;
            border-color: #00b3e3 !important;
            color: #fff !important;
            box-shadow: none !important;
        }

    `]
})
export class EditingComponent {
    
    public section: String;
    public fields: BehaviorSubject<Field[]>;
    public formConfiguration: FormConfiguration;

    public constructor(private modal: Modal) {
        this.fields = new BehaviorSubject<Field[]>([]);
        this.formConfiguration = new FormConfiguration();
    }

    public showSection(section: String): void {
        this.section = section;
    }

    public showJson(): void {
        let html = `
            <div class="card">
                <div class="card-body">
                    ${this.getJson()}
                </div>
            </div>`;

        this.modal.alert()
            .size('lg')
            .showClose(false)
            .title('JSON')
            .body(html)
            .open();
    }

    private getJson(): string {
        let json = `{
            "title": "${this.formConfiguration.title}",
            "imgLink": "${this.formConfiguration.imgLink}",
            "sections": [{
                "renderType": "FormSectionController",
                "controllersList": [{
                    "renderType": "FormRowController",
                    "controllersList": [{
                        "renderType": "FormColController",
                        "controllersList": [{
                            "renderType": "FormCardController",
                            "controllersList": [`;
                            
        json += this.getSerializedWithoutBrackets();
        json += `
                            , {
                                "renderType": "FormSubmitButtonController",
                                "label": "Enviar"
                            }
                            ]
                        }]
                    }]
                }]
            }],
            "gracias": "${this.formConfiguration.thanks}"
        }`;
        return json;
    }

    public getSerializedWithoutBrackets() {
        let json = JSON.stringify(this.fields.value);
        json = json.substr(1);
        json = json.substring(0, json.length - 1);
        return json;
    }

}