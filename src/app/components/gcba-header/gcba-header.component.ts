import { Component, Input } from "@angular/core";

@Component({
  selector: 'gcba-header',
  template: `
    <header id="header">
      <div class="border-gradient">
        <div id="title-container" style="margin-left:15px;">
          <span class="header-title"> Creador de formularios </span> 
        </div>
      </div>
    </header>`,
  styles: [`
    #ba-logo {
      width: 100px;
    }
  
    #header {
      height: 60px;
    }
    
    #title-container {
      margin-left:15px;
      margin-top: 7px;
    }

    .header-title {
      font-weight: 500;
      font-size: 1.7rem;
      font-family: "CHANEWEI", Helvetica, Arial, sans-serif !important;
      margin-bottom: -10px;
    }`
  ]
})
export class GCBAHeaderComponent {
}
