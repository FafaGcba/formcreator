import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { DragDropModule} from 'primeng/dragdrop';
import { PanelModule } from 'primeng/panel';
import { TableModule } from 'primeng/table';
import { SidebarModule } from 'primeng/sidebar';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { FormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/calendar';
import { EditingComponent } from './pages/editing/editing.component';
import { DetailComponent } from './pages/editing/assemble/components/detail/detail.component';
import { PreviewControlComponent } from './pages/editing/assemble/components/preview-control/preview-control.component';
import { ControlOptionsComponent } from './pages/editing/assemble/components/control-options/control-options.component';
import { AssembleComponent } from './pages/editing/assemble/assemble.component';
import { GCBAHeaderComponent } from './components/gcba-header/gcba-header.component';
import { ModalModule } from 'ngx-modialog';
import { BootstrapModalModule } from 'ngx-modialog/plugins/bootstrap';
import { DialogModule } from 'primeng/dialog';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { ParametersComponent } from './pages/editing/parameters/parameters.component';
import { ValidationsComponent } from './pages/editing/assemble/components/validations/validations.component';

let router: Routes = [
  //{ path: 'inicio', component: ResultadoComponent },
  { path:'', component: EditingComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    AssembleComponent,
    EditingComponent,
    DetailComponent,
    PreviewControlComponent,
    ControlOptionsComponent,
    GCBAHeaderComponent,
    ParametersComponent,
    ValidationsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(router),
    DragDropModule,
    BrowserModule,
    BrowserAnimationsModule,
    PanelModule,
    TableModule,
    SidebarModule,
    ConfirmDialogModule,
    FormsModule,
    CalendarModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
    DialogModule,
    OverlayPanelModule
  ],
  providers: [
    ConfirmationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
