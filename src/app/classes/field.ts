import { Validations } from "./validations";

export class Field {
    public renderType: string;
    public cmField: string;
    public name: string;
    public label: string;
    public placeholder : string;
    public validations: Array<Validations> = new Array<Validations>();
    public options: Array<string>;

    public asignProps(f: Field) {
      Field.asignProps(this, f);
    }

    public static asignProps(f1: Field, f2: Field) {
      f1.renderType = f2.renderType;
      f1.cmField = f2.cmField;
      f1.name = f2.name;
      f1.label = f2.label;
      f1.placeholder = f2.placeholder;
      f1.validations = f2.validations.filter(v => v == v);
      f1.options = f2.options.filter(o => o == o);
    }

  }