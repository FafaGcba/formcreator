export class Validations {
    public constructor(
        public type: String, 
        public params: Params) {
    }
} 

export class Params {
    public max: number;
    public maxLength: number;
    public min: number;
    public minLength: number;
    public errorMessage: string;
}